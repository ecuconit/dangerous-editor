# Dangerous Editor #

This is the README file to go with the Dangerous Editor, based on the [The Most  **Dangerous**  Writing App](https://www.squibler.io/dangerous-writing-prompt-app/).  It is a tool to let users practice writing.

### Description ###
This project lets the users set up a rich text editor environment of their liking, then write to their heart's contents.  The basic feature of dark mode and fullscreen are available throughout the application.  The major difference between this editor and its inspiration is the offline mode, and the ability to export the finished scripts to Word document.

This application is written HTML using [Bootstrap v5.0.0 beta 2](https://getbootstrap.com/docs/5.0/getting-started/introduction/), running on [jQuery v3.6.0](https://api.jquery.com/) and using [CKEditor 5](https://ckeditor.com/ckeditor-5/) as the backbone, running on [Electron v12.0.1](https://www.electronjs.org/docs).  Using [Visual Studio Community 2019](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16) as the IDE.

### Badges ###

No badges planned for now.

### Visuals ###

The basic visual of this application is using [Bootstrap 5.0](https://getbootstrap.com/docs/5.0/getting-started/introduction/)'s [Cover](https://getbootstrap.com/docs/5.0/examples/cover/) template.  Keeping things simple and most importantly, least distracting for the user.

Additional JS libraries are used to aid the UI, such as the following:

 - [Bootbox JS v5.5.2](http://bootboxjs.com/) - Used for modal, confirmation, alert, and other interactive features from the system to the user.
 - [Font Awesome v5.15.2](https://fontawesome.com/start) - For the icons and instructions.  When possible, written instruction shall be replaced by icons and symbols.
 - [Tippy JS v6.3.1](https://github.com/atomiks/tippyjs) - Bootstrap Tooltip has some issues, opted to use this instead.

All colors and themes are based on white (#FFFFFF) and black (#212529), a simplistic color scheme causes the least amount of distraction to the user.

An additional feature is to blur the text, so the user can't see what's been written.  Per request, three blur strength levels have been implemented for various degrees of visibility.

### Installation ###

The current build is based on [Electron v12.0.1](https://www.electronjs.org/docs), to build your own, pull a copy of this project and open it with [Visual Studio Community 2019](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16).  Make sure your Electron is properly referenced in Visual Studio: 

![Installing or updating Node modules via NPM in Visual Studio](https://bitbucket.org/ecuconit/dangerous-editor/downloads/NPM-Update.jpg)

Once [Node.JS v14.16.0](https://nodejs.org/en/download/) have been installed and the node modules have been updated, as shown above, reference the *electron.exe* to your local copy of Visual Studio's project settings.

![Electron configuration in Visual Studio](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Electron-Config-in-VS-Project.jpg)

Once the reference to electron had been established, just press `F5` to run the application.  Or alternatively, you can start the application using the command line, start one as shown below:
 
![Accessing command line from Visual Studio](https://bitbucket.org/ecuconit/dangerous-editor/downloads/VS-Command-Line.jpg)
 
Once the command-line editor started, enter `npm start` to run the application.

![Running the application from command line](https://bitbucket.org/ecuconit/dangerous-editor/downloads/NPM-Start-Command.jpg)
 
If you wish to build your own installer, run `npm run dist` to build the application into an installer, for easier redistribution:

![Building the installation file from command line](https://bitbucket.org/ecuconit/dangerous-editor/downloads/NPM-Build-Command.jpg)

Once the application is installed, it should be in the following location on your Windows 10 machine:

> C:\Users\[username]\AppData\Local\Programs\dangerous_editor

### Usage ###

Once the application is started, everything should be fairly simple to use, pretty self-explanatory.  Any additional explanation needed, just mouse over the icons or the info icon to read explanations.

#### Configuration Page ####

![Configuration page](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Config-Screen.jpg)

On the editor configuration page, two key elements have to be selected: *Idle Limit* and *Session Length*.

 - **Start Button** - Once the configurations below have been set to a user's liking, click on the start button to get started.
	 - This application exports the selected options to a configuration object, cached in the browser's *Local Storage* under the key of "*DangerousEditor-Config*".  Next time you start the application, the previously cached configuration will be applied.  So, if you want to start from scratch, wipe the "*DangerousEditor-Config*" in *Local Storage* via the *Open Dev Tool* function.
 - **Idle Limit** - The editor will automatically wipe the content of the editor if the user is idle for too long.  By default, the value is 5 seconds.  This is the main feature of the [The Most  **Dangerous**  Writing App](https://www.squibler.io/dangerous-writing-prompt-app/).
	 - Graphically, a red border will creep-in to give the user a visual indication that the editor is about to be timed out.  
	 - Two visual effects are implemented, for *Idle Limit* less than 3 seconds, a 1.5 seconds box-shadow effect will be used; for *Idle Limit* of 3 seconds or higher, the primary box-shadow effect will be triggered in the last 3 seconds prior to timed out, the effect lasts for 3 seconds.
 - **Session Length** - Once the session length has been met, the *Idle Limit* will be disabled, allowing the user all the time in the world to finish up the writing.  The session can be limited by *the number of words* entered or *amount of time* spent writing.
	 - Upon the session completion, the user will have the ability to use *Export to Word* feature to save their writings to an external Microsoft Word document.
 - **Blur Text** - This is an optional feature, it will make the entered text blurry, the idea is to not dwell on the past and being distracted by what was written.
	 - If the option is enabled, the slide bar next to the checkbox indicates the *Blur Strength*, the higher the value the more blurry.  Keep in mind too much blur may also be distracting to the user.
	 - The *Blur Text* effect can be toggled in the editor, but the *Blur Strength* has to be set on the configuration page.
 - **Spell Check** - By default, Chrome and many browsers have spell and grammar checkers built-in.  For those who do not want to see the correction mark, to avoid distraction, you can uncheck this option to disable the effect.
 - **Dark Theme** - When pressed, toggle the page between light theme and dark theme (by default).
	 - Keyboard shortcut `Ctrl + 3` 
 - **Full Screen** - When pressed, toggle the fullscreen and windowed mode (by default).
	 - Keyboard shortcut `F11` 
 - **Open Dev Tool** - When pressed, open the debug console for development testing purposes, or for users to mess with the code on their side.
	 - Keyboard shortcut `F12`

**Note:**

 1. Fullscreen and save feature is relying on the [Electron v12.0.1](https://www.electronjs.org/docs)'s native feature or  [Node.JS v14.16.0](https://nodejs.org/en/download/) modules.  In order to make them work, the [IPC](https://github.com/electron/electron/blob/master/docs/api/ipc-main.md) was implemented to facilitate inter-process communication, between Electron and the web.

#### Editor Page ####

When the page is entered, the cached configuration from the *Local Storage* will be fetched and applied.  The editor looks like a pretty standard WYSIWYG editor, so hopefully, everything is pretty self-explanatory. 

![Editor page](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Editor-Screen.jpg)

The editor won't start until you start typing, which the *Idle Timer* will be triggered to start the writing session.  The session won't end until the *Session Length* is reached by either *the number of words* had been entered or *amount of time* spent writing reached.

The footer of the editor indicates the current *number of words* and *session duration*, whichever is set as the *Session Length*, the maximum amount is displayed next to the current count.  As seen in the example above, a total of *11 seconds* had been spent out of a *5 minutes session*.

 - **Config Button** - When pressed, the user will be prompted to go back to *Config Page* or not.
	 - Keyboard shortcut `Esc`
 - **Reset Button** - When pressed, reset the editor back to the initialized state.  This resets the timers and wipes the editor's content.
	 - Keyboard shortcut `Ctrl + D` (for *delete*) or `Ctrl + R` (for *reset*)
  - **Save Button** - When pressed, export the content of the rich text editor as a Microsoft Word document.
	 - Keyboard shortcut `Ctrl + S`
	 - This function can only be accessed once the session is completed, as defined by the *Config Page*'s *Session Length* configuration, can be accessed via *Config Button*. 
 - **Spell Check** - When pressed, toggle the browser's built-in spell and grammar checker feature.
	 - Keyboard shortcut `Ctrl + 1` 
 - **Blur Mode** - When pressed, toggle the blur mode.
	 - Keyboard shortcut `Ctrl + 2` 
	 - This button can only be used to toggle the effect, to adjust the *Blur Strength*, you will need to set the value in the *Config Page*, which can be accessed via *Config Button*.
 - **Dark Theme** - When pressed, toggle the page between light theme and dark theme (by default).
	 - Keyboard shortcut `Ctrl + 3` 
 - **Full Screen** - When pressed, toggle the fullscreen and windowed mode (by default).
	 - Keyboard shortcut `F11` 
 - **Open Dev Tool** - When pressed, open the debug console for development testing purposes, or for users to mess with the code on their side.
	 - Keyboard shortcut `F12`
 - **CKEditor Features** - The editor uses CKEditor, with only a handful of toolbar enabled to serve minimum feature to ensure the least distraction.  If you want more or less, edit `Editor.html:L185~L186`.  Please see the CKEditor's official documentation for their keyboard shortcuts. 
	 - **Heading** - Effects such as *heading level* can be modified for block or group management.
	 - **Basic Format** - Effects such as *bold*, *italic*, *underline*, and *strike through* can be used to modify the basic text effect.
	 - **Font Format** - Effects such as *font size*, *font color*, and *font background color* are opted out of this build.  It doesn't play nice with the *Dark Theme* toggle and too much color can be very distracting.
	 - **Block Format** - The basic *block quote* with left-border line.  Also, the *indent* and *outdent* features can be used to move the entire block of text in or out.
		 - The *indent* is mapped to `Tab` key and the *outdent* is mapped to `Shift+Tab` key.
	 - **List Format** - The basic *bulleted list* and *numbered list* can be to organize items in a more manageable format.
	 - **History Tool** - The addition of *undo* and *redo* features allow for quick editing and undo editing.

**Note**

 1. Got to give another shout-out to [The Most  **Dangerous**  Writing App](https://www.squibler.io/dangerous-writing-prompt-app/) for the inspiration, it reminded me a lot of [Extreme Programming](https://en.wikipedia.org/wiki/Extreme_programming) sessions, great for beginners to practice.
 2. CKEditor 5 has a list of built-in [keyboard shortcuts](https://ckeditor.com/docs/ckeditor5/latest/features/keyboard-support.html), so if you want to create your own page keyboard event handlers or editor key event handlers, make sure to avoid the ones already recognized by the CKEditor. 

### Support ###

This is a hobby project created to aid writers out there who would like some fun new tool to aid their writing process.  If you would like to help, please help to debug this application and submit any bug reports or feature requests to the repo's [Issue Tracker](https://bitbucket.org/ecuconit/dangerous-editor/issues?status=new&status=open).  Please take some time to help break this application to the best of your capabilities.

Also, support your local book stores, pick up some books to inspire your next writing session.

### Roadmap ###

The current version of the application is fully implemented and usable.  Other than a few minor features and bug fixes, this project is not expected to proceed any further, unless there are lots of demands from the users.

### Contributing ###

Please consider attending [All Things Open](https://www.allthingsopen.org/) convention to learn about how to start your own open-sourced project or be a contributor.  Anyone is welcomed to pull and modify this application as they wish, just make sure you do not violate the terms of [MIT license](https://opensource.org/licenses/MIT) used by this application.

### Authors and Acknowledgment ###

This application is solely developed by [Kuan Chen](mailto:chenk@ecu.edu) of the East Carolina University. 

### License ###

This application uses tons of [MIT license](https://opensource.org/licenses/MIT) libraries, thus the huge installation package.  The original codes created by the author are free to distribute or reuse.

### Project Status ###

This project is now completed with v1.0.0 release.