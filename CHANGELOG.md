# Change Log #

This document tracks all the newly implemented features and more importantly the bug fixes.

### [1.0.1] 2021-03-23 ###

Minor bug fixes

**Added**

 - An icon have been added for the application.

**Changed**

 - Fullscreen now using Electron functionality, doesn't make much of a difference on usage, but runs smoother now

**Fixed**

 - *Editor.html*'s menu button now take user to *Config.html* rather than *Index.html*

### [1.0.0] 2021-03-22 ###

First release of the dangerous editor.  