// Source: https://www.codexworld.com/export-html-to-word-doc-docx-using-javascript/
$.Export2Word = function (element, filename = '') {
   var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title></head><body>";
   var postHtml = "</body></html>";
   //var html = preHtml + document.getElementById(element).innerHTML + postHtml;
   var html = preHtml + $(element).html() + postHtml;

   var blob = new Blob(['\ufeff', html], {
      type: 'application/msword'
   });

   // Specify link url
   var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

   // Specify file name
   filename = filename ? filename + '.doc' : 'document.doc';

   // Create download link element
   var downloadLink = document.createElement("a");
   document.body.appendChild(downloadLink);

   // Check to see if the link can be activated
   if (navigator.msSaveOrOpenBlob) {
      navigator.msSaveOrOpenBlob(blob, filename);
   } else {
      // Create a link to the file
      downloadLink.href = url;

      // Setting the file name
      downloadLink.download = filename;

      //triggering the function
      downloadLink.click();
   }

   // Delete afterward ^o^
   document.body.removeChild(downloadLink);
}