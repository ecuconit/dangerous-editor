// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain } = require('electron')
const config = require('./.vscode/launch.json')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
	// Create the browser window.
	mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		alwaysOnTop: false,
		titleBarStyle: 'hidden',
		// fullscreen: true,
		autoHideMenuBar: true,
		resizable: true,
		show: false,
		backgroundColor: '#312450',
		webPreferences: {
			// Electron v12 and above, to enable IPC
			nodeIntegration: true,
			contextIsolation: false,
			enableRemoteModule: true,
			webSecurity: false
		}
	})

	// and load the index.html of the app
	//mainWindow.loadURL(`file://${__dirname}/Index.html`); // .Net permission issue, work around	 
	mainWindow.loadURL(`file://${__dirname}/Config.html`)

	// Only show the window if it's ready
	mainWindow.on('ready-to-show', function () {
		mainWindow.show();

		// Open the DevTools.
		//mainWindow.maximize();
		//mainWindow.webContents.openDevTools()
	})

	// Emitted when the window is closed.
	mainWindow.on('closed', function () {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null
	})
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
	// On macOS it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', function () {
	// On macOS it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow()
	}
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

// Maximize the window
ipcMain.on('max-message', (event, arg) => {
	mainWindow.setFullScreen(arg)
	event.returnValue = arg
})

// Open the dev tool
ipcMain.on('open-dev-tool-message', (event, arg) => {
	// Open the DevTools
	mainWindow.webContents.openDevTools ()
	event.returnValue = true
})

// Download file as Word ^o^
ipcMain.on('word-export-message', (event, arg) => {
	console.log(arg) // prints "ping"
	event.reply('word-export-reply', 'done')
})