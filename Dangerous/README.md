# Dangerous Editor #

This is the README file to go with the Dangerous Editor, based on the [The Most  **Dangerous**  Writing App](https://www.squibler.io/dangerous-writing-prompt-app/).  It is a tool to let users practice writing.  

For application who wants to build their own binary file and use the application, please use the repo's [readme file](https://bitbucket.org/ecuconit/dangerous-editor/src/main/README.md).  This document is designed for developers who want to modify this solution.

### Installation ###

#### Prerequisits ####

This project is built as an Electron application using Visual Studio as IDE, more specifically the following versions of the software:

 - [Visual Studio Community 2019](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16) - Make sure you have *Node.js Development* selected in Visual Studio's workloads
 - [Node.JS v14.16.0](https://nodejs.org/en/download/) - Once Node is installed and NPM is registered to Windows' *Environmental Variables*, pull the following modules, or use *Install npm packages* feature of Visual Studio:
   - [Electron v12.0.1](https://www.electronjs.org/docs)
   - [Electron Builder v22.10.5](https://github.com/electron-userland/electron-builder)
   - No an additional module, but here is the documentation on the built-in [IPC (inter-process communication)](https://github.com/electron/electron/blob/master/docs/api/ipc-main.md) for Electron

#### Additional Libraries ####

This application uses a bunch of open-sourced libraries.  The distribution version of the libraries are all included in the `.\vendor\` folder but if you need to match versions, this is a list of the libraries used:

 - [Bootbox JS v5.5.2](http://bootboxjs.com/) - Used for modal, confirmation, alert, and other interactive features from the system to the user.
 - [Bootstrap v5.0.0 beta 2](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
 - [CKEditor 5](https://ckeditor.com/ckeditor-5/)
   - [Keyboard shortcut](https://ckeditor.com/docs/ckeditor5/latest/features/keyboard-support.html) documentation
 - [Export2Word](https://www.codexworld.com/export-html-to-word-doc-docx-using-javascript/)
   - It is not really being used, parts of the codes were used.
 - [Font Awesome v5.15.2](https://fontawesome.com/start) - For the icons and instructions.  When possible, written instruction shall be replaced by icons and symbols.
 - [jQuery v3.6.0](https://api.jquery.com/)
   - Additional jQuery UI was used for tooltip but was removed in place of Tippy JS
 - [Tippy JS v6.3.1](https://github.com/atomiks/tippyjs) - Bootstrap Tooltip has some issues, opted to use this instead.
   - Instruction on [Constructor](https://atomiks.github.io/tippyjs/v6/constructor/)
   - Public [distribution folder](https://unpkg.com/browse/tippy.js@6.3.1/dist/)

Some of these libraries are included in the *package.json* file, honestly more of them should be too.  Below show the package file where Electron files are included. 

![Electron project inclusion in Package.json](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Package-File.jpg)

Alternatively, Visual Studio provides an easier way to include libraries by using the graphical interface by right-clicking on *Solution > Dangerous > npm* in the *Solution Explorer*.  Select *Install New npm Packages* to get the interface below.  Make sure to check the "*Add to package.json*" option, making it easier for the later updates.

![Install Node modules via GUI](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Install-NPM-Package.jpg)

### Usage ###

Once all of the required applications have been installed and optional additional libraries downloaded, you can now start to mess with the codes.

![Electron configuration in Visual Studio](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Electron-Config-in-VS-Project.jpg)

The Electron file is located at `.\Dangerous\node_modules\electron\dist\electron.exe`.  As shown in the image above, the startup script is *server.js*

#### Server.js ####

*Server.js* is the launch point for Electron to start.  Electron creates a window that isn't *fullScreen* by default but allows *resize*, letting the user decide how to use this application.  Also, disable *show* until the windows are created. 

```js
mainWindow = new BrowserWindow({
	width: 800,
	height: 600,
	alwaysOnTop: false,
	titleBarStyle: 'hidden',
	// fullscreen: true,
	autoHideMenuBar: true,
	resizable: true,
	show: false,
	backgroundColor: '#312450',
	webPreferences: {
		nodeIntegration: true,
		contextIsolation: false,
		enableRemoteModule: true,
		webSecurity: false
	}
})
```

To enable IPC, make sure *nodeIntegration* is set to *true* and to make sure dynamic content can be added correctly, let's set *webSecurity* to *false*.

Once the window is ready, we then show the window.  Optionally, the window can be made maximized or the dev tool opened if necessary, help with debugging.

```js
mainWindow.on('ready-to-show', function () {
	mainWindow.show();

	// Open the DevTools.
	//mainWindow.maximize();
	//mainWindow.webContents.openDevTools()
})
```

To use Electron functionalities, commands need to reside in the script ran by Electron's main process, not the renderer process (web page).  Below there are two event handlers that listen to "*max-message*" to toggle the maximized state and "*open-dev-tool-message*" message to open the debug tool.  Both of these methods can be called by JavaScript in the renderer process via IPC.

```js
// Maximize the window
ipcMain.on('max-message', (event, arg) => {
	mainWindow.setFullScreen(arg)
	event.returnValue = arg
})

// Open the dev tool
ipcMain.on('open-dev-tool-message', (event, arg) => {
	// Open the DevTools
	mainWindow.webContents.openDevTools ()
	event.returnValue = true
})
```

If further functionalities such as export to Microsoft Word document or PDF is needed, their IPC message handler needs to be added here, so JS can utilize other Node modules talking to the shell in a way that web pages can't.

#### Shared JavaScript (Shared.js) ####

Some scripts are being used by both *Config.html* and *Editor.html*, so those scripts are moved to `.\js\shared.js`, for easier code management.  

Same thing with *CustomFunction.js*, it's a collection of more commonly used functionalities that I use being tossed into a jQuery library, easy for reuse.  Simply adding the script to gain access to methods like:

```js
var isEmpty = $.isNullOrEmpty(string);
var value = $.urlParam(parameter);
var randomID = $.UUIDv4();
```

This library file is mostly not being used, but it's useful for me to have these methods handy.

The *Shared.js* is organized by their usage:

```js
// Page Initialization Methods
{
	// Shared initialization method
	function SharedInitialization();
}

// Helper Method
{
	// Additional methods that would be useful for both pages
}

// Local Storage Functionalities
{
	// Methods used for writing to the browser's local storage and read from the storage
}
```

On both *Config.html* and *Editor.html*, after the initial component initializations, call the *SharedInitialization()* method to initialize additional components existed on both pages.  Such as the tooltip:

```js
tippy('[data-bs-toggle]', {
	content: (reference) => reference.getAttribute('data-title'),
	allowHTML: true,
});
```

Selecting all elements with *data-bs-toggle* parameter, set the tooltip content to be the value set by *data-title*.  For the ease of use, always *allowHTML* so I can be fancy with writing instructions using tooltip.

Other components that are being initialized are the shared button event handlers such as: 

```js
// Light theme button click event
$('#light').bind('click', function () { ... });

// Full screen button click event
$('#fullscreen').bind('click', function () { ... });

// Configuration page button click event
$('#devTool').bind('click', function () { ... });

...
```

To initialize the page with cached configuration, I have one block of script used to trigger UI initialization and data cache.  This feature utilized the *GetEntry (localStorageKey)* method in this same document.  Below is an example of what the cached configuration payload looks like:

```json
DangerousEditor-Config: {
	BlurButton: true
	BlurStrength: "1"
	FullScreen: false
	IdleLimit: "5"
	LightTheme: false
	SessionLimit: "5"
	SessionType: "minutes"
	SpellCheck: false
}
```

Once the cached configuration is fetched from the browser's local storage, I can toggle display and bring back the last saved configuration.

```js
// Load configuration
var cache = GetEntry(config.ConfigName());
if (cache != null) {
	// Setup UI based on the configuration
	if (cache.LightTheme) // Switch the application to use light theme
	if (cache.BlurButton) // Switch the editor to blur mode
	...

	// Update the process config
	config.IdleLimit = cache.IdleLimit; // Cache the idle timer duration
	config.Session.Type = cache.SessionType; // Cache the session length type
	config.Session.Limit = cache.SessionLimit; // Cache how long the session lasts
	...
}
```

If more options are added to the configuration, please see the *Configuration Page* section to see how to save option values to cache, so the script above can fetch the configuration from the cache and initialize the display.

The last part of shared page initialization involves a key-down event handler, to allow for keyboard shortcut access to application features.  Since this application uses CKEditor 5, it is best to avoid their established [keyboard shortcut](https://ckeditor.com/docs/ckeditor5/latest/features/keyboard-support.html) as documented above.

```js
// Detect keydown
$(document).on('keydown', (data) => {
	switch (data.keyCode) {
		case 27: 
			// Escape key pressed, trigger escape UI button click
			...
			return;
		case 68:
		case 82:
			if (data.ctrlKey) {
				// Ctrl + D/R, trigger state reset
				...
			}
			return;
		...
		case 123:
			// F12, debug console
			...
			return;
	}
};
```

It is important for every handled keyboard event to handled purely in this code block, so it wouldn't trigger browser level or system-level events.  So each key or key combination handler needs to add the following code unless there is a reason you would like for the other event(s) to be fired as well:

```js
data.preventDefault();
data.stopPropagation();
```

There aren't many helper methods in this project, most of which have already been created as part of *CustomFunction.js*.  As part of template design, I kept this region of code, so if there are some helper methods, feel free to add them here.

```js
// Helper Method
{
	// Proceed to another page
	function Proceed(uri) {
		window.location.assign(uri);
	}

	...
}
```

The local storage part of the code is pretty standard.  It requires each page to have a configuration for the application code (`config.ProjID`), this allows the project name to be added as the header of all storage keys involved with this project.  This prevents collection with other applications.

```js
{
	// Get all cached entries
	function GetAllEntries();

	// Get all cached keys
	function GetAllKeys();

	// Clear a cached entry if Id is provided, clear all otherwise
	function ClearCache(key);

	// Add an entry to cache, option to overwrite key
	function Push(entry, key = null);

	// Get an entry from the cache
	function GetEntry(key);
}
```

This block of code allows for eacy access to the browser's local storage.  You can access the storage, given the application code is set on each page, by doing the following:

```js
var callback = Push(jsob, 'some_key_name'); // Save a JSON object to 'some_key_name'
var jsob = GetKey('some_key_name'); // Get the JSON object out of local storage with the key of 'some_key_name'
ClearCache('some_key_name'); // Wipe the cached data with the key of 'some_key_name'
```

Feel free to use this block of code to make other browser-based projects easier.

#### Custom Styling (style.css) ####

Only adding the styling codes to overwrite whatever [Bootstrap v5](https://getbootstrap.com/docs/5.0/getting-started/introduction/) doesn't already provide.  Other than the usual HTML element styling, a couple of things noteworthy are documented here.

When an element is disabled, we dim it, making the text gray.  When an element is not valid, the red outline is added to indicate such.

``` css
.dim {
	color: #b9b9b9;
}

.error input[type="number"], .error input[type="radio"] {
	border-color: red;
	border-width: 2px;
}
```

As part of foundational feature, when the editor is timing out, a creeping red border appears by CSS.  The class was taken and modified from [The Most  **Dangerous**  Writing App](https://www.squibler.io/dangerous-writing-prompt-app/), paying homage to their script, but modified to be more visible.

``` css
/* Idle delay effect */
.danger {
	-webkit-box-shadow: inset 0px 0px 60px 4px #f45653;
	box-shadow: inset 0px 0px 60px 4px #f45653;
	-webkit-transition: -webkit-box-shadow 3s ease-in;
	transition: -webkit-box-shadow 3s ease-in;
	-o-transition: box-shadow 3s ease-in;
	transition: box-shadow 3s ease-in;
	transition: box-shadow 3s ease-in, -webkit-box-shadow 3s ease-in;
}
.danger-short {
	-webkit-box-shadow: inset 0px 0px 60px 4px #f45653;
	box-shadow: inset 0px 0px 60px 4px #f45653;
	-webkit-transition: -webkit-box-shadow 1.5s ease-out;
	transition: -webkit-box-shadow 1.5s ease-out;
	-o-transition: box-shadow 1.5s ease-out;
	transition: box-shadow 1.5s ease-out;
	transition: box-shadow 1.5s ease-out, -webkit-box-shadow 3s ease-out;
}
.blink_me {
	animation: blinker 1s linear infinite;
}
@keyframes blinker {
	50% {
		opacity: 0;
	}
}
```

The original [The Most  **Dangerous**  Writing App](https://www.squibler.io/dangerous-writing-prompt-app/)'s styling is intended to be timed out in 5 seconds, so the animation lasts for 3 seconds.  For this editor, the *Idle Timer* can be any duration, so an additional ```danger-short``` class is added, for any *Idle Timer* duration between 1 ~ 3 seconds long.

To overwrite the default [CKEditor 5](https://ckeditor.com/ckeditor-5/) styling, additional CSS classes are added for dark theme and light theme.  For these color modes, we basically just toggle the two colors of black (#FFFFFF) and white (#212529) between color and background-color.  This aspect will be much better if a variable is used, however, since it's not widely used, hard-coding here is fine.

``` css
/* Light and dark theme toggle */
.bg-dark .ck-editor__main {
	color: #FFFFFF !important;
}
.bg-dark .ck-editor__editable {
	background-color: #212529 !important;
}
.bg-light .ck-editor__main {
	color: #212529 !important;
}
.bg-light .ck-editor__editable {
	background-color: #FFFFFF !important;
}
```

To make the blurry mode works, however, the effect is accomplished by making the text transparent and adding blurry drop-shadow.  Making things complicated, the color of the drop-shadow differs in light and dark themes.  For even more fun, 3 levels of blurry effects (blurry, blurrier, and blurriest) are implemented here.

``` css
/* Blurring mode */
.blurriest-text .ck-editor__editable, .blurrier-text .ck-editor__editable, .blurry-text .ck-editor__editable {
	color: transparent;
}
.bg-dark .blurriest-text .ck-editor__editable {
	text-shadow: 0 0 8px rgba(255,255,255,0.25);
}
.bg-dark .blurrier-text .ck-editor__editable {
	text-shadow: 0 0 6px rgba(255,255,255,0.38);
}
.bg-dark .blurry-text .ck-editor__editable {
	text-shadow: 0 0 5px rgba(255,255,255,0.5);
}
.bg-light .blurriest-text .ck-editor__editable {
	text-shadow: 0 0 8px rgba(33,37,41,0.25);
}
.bg-light .blurrier-text .ck-editor__editable {
	text-shadow: 0 0 6px rgba(33,37,41,0.38);
}
.bg-light .blurry-text .ck-editor__editable {
	text-shadow: 0 0 5px rgba(33,37,41,0.5);
}
```

When applying these dimmed, error, danger, dark, light, and blur effects, jQuery is used to dynamically adding and removing classes for the visual effects.

#### Configuration Page (Config.html) ####

The editor side of the project is fairly straightforward, setting up the editor and store the values into the browser's local storage.  So the next time when the editor starts, the previously stored values can be pulled and reused, continuing the previous session as minimal effort.

The configurations are split into two sections, as shown in the screenshot below:

![Configuration sections](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Editor-Sections.jpg)

##### Navigation Bar #####

The top section, the navigation bar, persists across the project, so all shared functionalities (*Dark Theme*, *Full Screen*, and *Dev Tool*) resides here for easier reusability.  To make things simple, use appropriate icons using [Font Awesome](https://fontawesome.com/start) to avoid taking up unnecessary screen space for instructions.

The more complicated configuration is being represented as a form, allowing for custom inputs such as text, number, and such.  

Note that *Spell Check* can easily be presented as an icon much like the top navigation bar, it's a simple toggle.  However, since it only affects the editor page, it can, but doesn't need to be migrated to the navigation bar.

The interface for both are following [Bootstrap v5](https://getbootstrap.com/docs/5.0/getting-started/introduction/) template, so the navigation bar resides in the following section:

``` html
<nav class="nav nav-masthead justify-content-center float-md-end">
	<!-- Header icons -->
	<a class="nav-link" id="light" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="bottom" data-title="<b>[Ctrl] + [3]</b> -> Toggle between light theme and dark theme"><i class="far fa-lightbulb"></i></a>
	<a class="nav-link" id="fullscreen" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="bottom" data-title="<b>[F11]</b> -> Toggle full screen mode or windowed mode"><i class="fas fa-expand-arrows-alt"></i></a>
	<a class="nav-link" id="devTool" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="bottom" data-title="<b>[F22]</b> -> Open the dev tool"><i class="fas fa-terminal"></i></a>
</nav>
```

Note that ```data-bs-toggle="tooltip"``` and ```data-title="{instruction}"``` are included, using the tooltip to display instructions rather than having them in page.

I know that it's not fashionable to use ID with the new HTML standard, but they made jQuery selector so much easier.

##### Configuration Form #####

As to the form, it's also using [Bootstrap v5](https://getbootstrap.com/docs/5.0/getting-started/introduction/) form.  The standard label and form elements, but with an additional info icon for the tooltip description.

``` html
<form class="lead text-start">
	<!-- Config form -->
	<label for="idle" class="form-label">Idle Limit <sup><i class="fas fa-info-circle" data-bs-toggle="tooltip" data-bs-placement="top" data-title="Wipe content after idled for <em>x</em> seconds"></i></sup></label>
	<div class="input-group mb-3">
		<input type="number" class="form-control" id="idle" min="3" value="5" aria-describedby="basic-addon3">
		<span class="input-group-text inputEnder">Seconds</span>
	</div>
	...

	<!-- Submit button -->
	<button id="submit" type="button" class="btn btn-primary">Start</button>
</form>
```

A good chunk of the navigation bar button scripts resides in the **Shared JavaScript** region above, as part of the *SharedInitialization* method.

``` js
// Shared initialization method
function SharedInitialization() {
	// Button event handler
	$('#buttonID').bind('click', function () { 
		// Toggle state and icon
		config.ButtonName.state = !config.ButtonName.state;
		$(this).find('svg').attr('data-icon', (config.ButtonName.state ? config.ButtonName.onIcon : config.ButtonName.offIcon));
		
		// Actual work here
		...
	});
	// More buttons...
}
```

Most icons work based on the template above, updating the button state in the cached config object, which states the [Font Awesome v5](https://fontawesome.com/start) icons to be used for the on and off state of the button.  The config object looks something like below.

``` js
var config = {
	Button: {
		state: false,
		onIcon: '{fa-icon-on}',
		offIcon: '{fa-icon-off}',
		additionalValue: {value}
	},
	Option1: {value1}
	...
	App: 'DangerousEditor',
	ConfigName: function () {
		return config.App + '-Config';
	},
	EditorURI: 'Editor.html',
};
```

Note that [Font Awesome v5](https://fontawesome.com/start) can no longer swap the displayed icon dynamically by updating the class of the ```<i class="fa {fa-icon-on}" aria-hidden="true"></i>``` likein version 4.7, instead, swapping the icon by script had to use ```$(this).find('svg').attr('data-icon', '{fa-icon}');``` to change the icon and ```$(this).find('svg').attr('data-prefix', '{fas | fab | far | fal | fad}');``` to change the style.

So a more comprehensive way to handle configuration is to have style and icon parameters added separately, having the template changing both based on the need.  However, it's a bit of overkill for a project of this scope.

All other button initialization should be in the page's initialization script.  As documented above, the shared initialization methods (```SharedInitialization()```) will be called afterward.

```js
// On page ready, initialize
$(document).ready(function () {
	Initialize();
})

// Initialize the current form
function Initialize() {
	// Value change event handler
	$('#controlID').on('change', function () {
		// Handle event
		...
	});
	// More events
	...

	// Submit button event handler
	$('#submit').bind('click', function () {
		// Check to see if the form is completed, if so, proceed to editor
		if (VerifyForm()) {
			Push(FromForm(), config.ConfigName());
			Proceed(config.EditorURI);
		}
	});

	// Used shared library
	SharedInitialization();
}
```

Once the form elements have been handled, the submit button is bound with a handler.  A simple action of verifying the form, if verified, push the configuration into the local cache (via **Shared JavaScript**'s helper's *Local Storage* methods) and proceed (via **Shared JavaScript**'s helper's *Proceed* method) to the next page (indicated by *config.EditorURI*).

To verify the form, check to see if all-numeric form elements contain proper values.  The second part of the check is conditional check, only verify the numeric box if the checkbox is checked.  The algorithm is as below.

``` js
// Check to see if the form is verified
function VerifyForm() {
	var verified = true;

	// Check for numeric box
	$('#idle').each(function (i, e) {
		if (!$.isNumeric($(this).val())) {
			verified = false;
			$(this).parent().addClass('error');
		} else {
			$(this).parent().removeClass('error');
		}
	});

	// Conditional check
	var hasSelected = false;
	$('input[type="radio"][name="session"]').each(function (i, e) {
		if ($(this).prop('checked')) {
			hasSelected = true;
			// Check for associated values
			if (!$.isNumeric($(this).parent().parent().children('input[type="number"]').val())) {
				$(this).parent().parent().addClass('error');
			} else {
				$(this).parent().parent().removeClass('error');
			}
		}
	});
	if (!hasSelected) {
		// Nothing selected
		verified = false;
		$('input[type="radio"][name="session"]').each(function (i, e) {
			$(this).parent().parent().addClass('error');
		});
	}

	return verified;
}
```

To indicate an error, rather than using text or a pop-up, I love to just use a red outline to indicate the error.  So, any form element containing the error will have an ```error``` class added to the HTML tag.  The styling is included in the **Custom Styling** document.  Any element that doesn't have an error, just remove the ```error``` class.

``` css
.error input[type="number"], .error input[type="radio"] {
	border-color: red;
	border-width: 2px;
}
```

Dump the form into a JSOB (JavaScript OBject), the ```FromForm()``` method was used.  Fetching values from to form elements and local config values, turning it into a JSOB, which can then be dumped to console or save to local storage via the **Shared JavaScript**'s helper's *Local Storage* methods as documented above.

``` js
function FromForm() {
	return {
		LightTheme: config.LightButton.state,
		FullScreen: config.FullButton.state,
		IdleLimit: $('#idle').val(),
		SessionType: $('input[type="radio"][name="session"]:checked').val(),
		SessionLimit: $('#' + $('input[type="radio"][name="session"]:checked').val()).val(),
		BlurButton: $('#blurOption').prop('checked'),
		BlurStrength: $('#blurText').val(),
		SpellCheck: $('#spellcheckOn').prop('checked')
	};
}
```

Once the user has interacted with the form elements, click on the submit button, the values will be dumped from the form, save to local storage, then proceed to the editor page.

#### Editor Page (Editor.html) ####

##### Navigation Bar #####

Like the **Configuration Page**, the navigation bar at the top shows quick access to functionalities to the system, which can also be activated by keyboard shortcut.  The keys are documented as part of mouse over tooltip as usual.

![Editor page of the editor](https://bitbucket.org/ecuconit/dangerous-editor/downloads/Editor-Screen.jpg)

The scripting is very much like in the **Configuration Page**, icons are added to the navigation bar of the header tag as below:

``` html
<nav class="nav nav-masthead justify-content-center float-md-end">
	<!-- Header icons -->
	<a class="nav-link" id="config" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-title="<b>[F1]</b> -> Click to discard current editor and go back to configuration page."><i class="fas fa-cog"></i></a>
	<div class="divider">|</div>
	<a class="nav-link" id="reset" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-title="<b>[Ctrl] + [D]/[R]</b> -> Erase current editor and reset the timers"><i class="fas fa-eraser"></i></a>
	<a class="nav-link disabled" id="save" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-title="<b>[Ctrl] + [S]</b> -> Save current editor to external file"><i class="far fa-save"></i></a>
	<div class="divider">|</div>
	<a class="nav-link" id="spellCheck" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-title="<b>[Ctrl] + [1]</b> -> Toggle the built-in spell checker feature by the browser"><i class="fas fa-spell-check"></i></a>
	<a class="nav-link" id="blur" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-title="<b>[Ctrl] + [2]</b> -> Toggle the blur mode, to hide what's been written"><i class="far fa-eye-slash"></i></a>
	<a class="nav-link" id="light" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-title="<b>[Ctrl] + [3]</b> -> Toggle between light theme and dark theme"><i class="far fa-lightbulb"></i></a>
	<a class="nav-link" id="fullscreen" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="bottom" data-title="<b>[F11]</b> -> Toggle full screen mode or windowed mode"><i class="fas fa-expand-arrows-alt"></i></a>
	<a class="nav-link" id="devTool" href="#" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="bottom" data-title="<b>[F22]</b> -> Open the dev tool"><i class="fas fa-terminal"></i></a>
</nav>
```

Note that a ```<div class="divider">|</div>``` is used to indicate a vertical divider, separating each block of functionalities.  The same tooltip parameters are added, so the *SharedInitialization* method can add activate the tooltip and button interactions.  Additional button event handlers are added to the rest of the page constructor, in the document-ready event handler.

``` js
// On page ready, initialize
$(document).ready(function () {
	Initialize();
})

// Initialize the current form
function Initialize() {
	// Used shared library
	SharedInitialization();

	// Additional event handlers here
	...
}
```

Note that currently all of the button events are handled by the *SharedInitialization* method, but if additional buttons to be added, that's unique to **Editor Page** but not to **Configuration Page**, add the event handlers here.  If it can be added to *SharedInitialization* in **Shared JavaScript** without messing up **Configuration Page**, then it wouldn't hurt for all of the navigation bar functionalities to be centralized in *SharedInitialization*.


##### Editor Body #####

Other than just initializing the navigation bar, 

The body section is a standard rich text editor powered by [CKEditor 5](https://ckeditor.com/ckeditor-5/).  To minimize distraction, only the basic tools (formatting, bold, italic, underline, indent, list) have been enabled.

``` js
// Initialize the current form
function Initialize() {
	// Button event handlers
	...

	// Check for resize event
	window.onresize = function () {
		EditorResize();
	};

	// Initialize editor
	ClassicEditor
		.create(document.querySelector('#editor'), {
			// Initialization
			toolbar: {
				items: ['heading', '|', 'bold', 'italic', 'underline', 'strikethrough', '|', 'fontSize', 'fontColor', 'fontBackgroundColor',
					'|', 'blockquote', '|', 'outdent', 'indent', '|', 'bulletedList', 'numberedList', '|', 'undo', 'redo', 'wordCount']
			},
			indentBlock: {
				offset: 1,
				unit: 'em'
			},
			wordCount: {
				onUpdate: stats => {
					// Prints the current content statistics
					$('#wordCount').html(stats.words);
					$('#charCount').html(stats.characters);

					// Check for trigger event
					inputType(stats.words);
				}
			},
			config: {
				ui: {
					height: '300px'
				}
			}
		})
		.then(res => {
			// Editor reference
			editor = res;
			const view = editor.editing.view;
			const viewDocument = view.document;

			// Initialize editor size
			EditorResize();

			// Bind custom key, avoid default keys https://ckeditor.com/docs/ckeditor5/latest/features/keyboard-support.html
			editor.keystrokes.set('Tab', (keyEvtData, cancel) => {
				//console.log('Tab has been pressed');
				//editor.execute('input', { text: config.TabChar });
				editor.execute('indentBlock');
				cancel();
			});
			editor.keystrokes.set('Shift+Tab', (keyEvtData, cancel) => {
				//console.log('Shift+Tab has been pressed');
				editor.execute('outdentBlock');
				cancel();
			});

			// Initialize state
			StateUpdate(state.Initialized);
		})
		.catch(error => {
			// Constructor error handler
			console.error(error);
		});
}
```

The first part of the script is to handle the window resize event handler.  Because [CKEditor 5](https://ckeditor.com/ckeditor-5/) does not have a built-in resizing feature, an event handler is used to capture the resize event, recalculate the size for the editor, then use the *EditorResize* method to dynamically resize the [CKEditor 5](https://ckeditor.com/ckeditor-5/).

Because [CKEditor 5](https://ckeditor.com/ckeditor-5/) overwrites classes and values by the library, so just by selecting the ```.ck-editor__editable``` control to overwrite the element's height attribute would not work.  So what the script does is to take the current window height, subtract by the space that page header, page footer takes up, and the CK editor's toolbar (sticky panel).  An additional static offset value is added to the *config* variable, to take into account the additional margin and padding spaces.

``` js
// Update CKEditor size
function EditorResize() {
	// Edit the style node
	var editorHeight = $(window).height() - $('header').outerHeight() - $('footer').outerHeight() - $('.ck-sticky-panel').outerHeight() - config.EditorOffset;
	if (editorStyle != null)
		editorStyle.remove();
	editorStyle = document.createElement('style')
	editorStyle.innerHTML = '.ck-editor__editable { height: ' + editorHeight + 'px !important }';
	document.body.appendChild(editorStyle);
}
```

JavaScript is used to dynamically wipe the previous instance of CSS script referenced by the *editorStyle* variable.  Then a new style tag is created with the corresponding height information, then injected back to the document.

The second part of the script is to initialize the [CKEditor 5](https://ckeditor.com/ckeditor-5/) as an HTML element.  The initialization method is split into three seconds, for promises.

 * *Create* - In this section, [CKEditor 5](https://ckeditor.com/ckeditor-5/) is initialized by the control identified by the ID of ```#editor```, the second parameter being the configuration which to initialize the editor.
    * *Toolbar* - A collection of [CKEditor 5](https://ckeditor.com/ckeditor-5/) tools to be used to aid writing to see what they are and how to use them, see the [Editor toolbar](https://ckeditor.com/docs/ckeditor5/latest/features/toolbar/toolbar.html) documentation.
    * *IndentBlock* - Set how wide the indention block is.
    * *WordCount* - Used to configurate the additional plug-in of [Word Count](https://ckeditor.com/docs/ckeditor5/12.3.0/features/word-count.html).  
       * Whenever the editor's word count changes, the *onUpdate* method will be promised.  In which case, display the current number of words and characters to display, then call the *inputType* handler to update input word counts.
    * *Config* - Configuration to the editor itself.  Usually, this is where parameters like height, width, and others would be defined.
 * *Then* - On the [CKEditor 5](https://ckeditor.com/ckeditor-5/) initialization completion, a promise is made and this event handler is fired.  This section is to initialize how the editor is used.
    * First, the ```editor``` reference is kept in global reference, so state change handler and input event handler can refer to them easily.
    * Editor initialized, let's initialize the height based on the current window size by calling the ```EditorResize``` method.
    * [CKEditor 5](https://ckeditor.com/ckeditor-5/) allows for keystroke binding for additional effects.  In this project, some custom features are proposed but most haven't been implemented.
       * *Tab* - Indention, unlike Microsoft Words, HTML doesn't really do tabs as an indention object.  A custom ```<span class="indent"></span>``` have been inserted tried, more testing is needed to make it work like in Words.  So, instead, this just calls the built-in [CKEditor 5](https://ckeditor.com/ckeditor-5/)'s indention feature.
       * *Shift+Tab* - Outdention, like indention, right now this keystroke just calls the outdent feature of the [CKEditor 5](https://ckeditor.com/ckeditor-5/).
       * *Backspace* - If the custom indention would have worked, things would be fine.  The alternate plan was to manually insert *x*-number of empty spaces in place of indention.  In that case, a backspace will delete *x*-number of empty spaces if they exist before the cursor.
       * *Delete* - Like backspace, a deletion would have removed *x*-number of empty spaces after the cursor.
 * *Catch* - In case there is an exception in editor initialization, this is the section which to handle the error.  Without having a full error display, the error message is only added to the console for now.

The application starts in an error state until the [CKEditor 5](https://ckeditor.com/ckeditor-5/) initialization is completed.  
Once [CKEditor 5](https://ckeditor.com/ckeditor-5/) have been initialized, a series of timer event handlers and keystroke event handlers will be fired to causing the editor state transition, utilizing the ```StateUpdate (setTo)``` method.  The state of the application is then set to *initialized* by calling ```StateUpdate(state.Initialized);``` transition. 

###### Application State Change ######

This editor has two completion modes: time or words.  When the user has typed certain minutes without timed out or having typed a certain number of words, the session will end.  Which type of session completion is set by *Session Length* in the **Editor Page**, a number of minutes or words are chosen by the users on the page, cached to local storage, then brought back to the *config* variable on this page by *SharedInitialization*.

``` js
// Initialize the current form
function Initialize() {
	...

	// Check for completion type
	$('#timeLimit').html('');
	if (config.Session.Type == 'minutes') {
		tickType = TickAndBail;
		inputType = Word;
		$('#timeLimit').html(`/${config.Session.Limit}:00`);
	} else {
		tickType = Tick;
		inputType = WordAndBail;
		$('#wordLimit').html(`/${config.Session.Limit} Words`);
	}

	...
}
```

This application behaves on state transition, indicate by the ```cur``` variable that uses the state enumerator ```var state = { Error: -1, Initialized: 0, Started: 1, Interacted: 2, Warning: 3, TimedOut: 5, Completed: 10 };``` to identify the current state and what state to set to.

![Dangerous Editor State Diagram](https://bitbucket.org/ecuconit/dangerous-editor/downloads/State_Diagram.png)

The application starts in an error state, until the [CKEditor 5](https://ckeditor.com/ckeditor-5/) initialization is completed, which is set to be *Initialized*.  Whenever a keystroke is entered the application switches to the *Started* state; whenever the application timed or reset, it goes back to the *Initialized* state again.

*Started* state remains in place until either reset to *Initialized* state or when *Completed* state, whenever the word or time out is reached.

The framework for state transition is fairly simple, template shown below:

``` js
// State change event handler
function StateUpdate(setTo) {
	switch (setTo) {
		case state.State1:
			// Validation and verification
			if (impossible(cur, setTo)) {
				console.log('Invalid state transition.');
				return;
			}

			// Perform actions
			...
			break;
		case state.State2:
			...
	};
	// Update current state
	cur = setTo;
}
```

When the transition between states, always check to see if *cur* -> *setTo* is possible.  If not, bail out.  Otherwise, perform the action and then update the *cur* state market to what was set to be.

For this project, we set up the following state transitions, mostly to interact with the timers and set/reset user interface using CSS class documented in **Custom Styling** section.

``` js
case state.Initialized:
	// Wipe timer
	clearTimeout(idleTimer);
	clearInterval(clock);
	// Wipe content
	editor.setData('<p></p>');
	// Kill animation
	$('body').removeClass('danger danger-short');
	$('#wordLimit, #timeLimit').removeClass('blink_me');
	$('#save').addClass('disabled');
	break;
case state.Started:
	// Starting, initialize timers
	startTime = new Date();
	clock = setInterval(tickType, 1000);
	break;
case state.Warning:
	// Count down the last 3 seconds, switch to timeout
	idleTimer = setTimeout(StateUpdate, 3000, state.TimedOut);
	$('body').addClass('danger');
	return;
case state.TimedOut:
	// Wipe out, reinitialize
	StateUpdate(state.Initialized);
	return;
case state.Completed:
	// Check for entrance
	if (cur != state.Started) return;

	// Completed, kill timer
	clearTimeout(idleTimer);
	// Kill animation
	$('body').removeClass('danger danger-short');

	// Display secondary animation
	$('#wordLimit, #timeLimit').addClass('blink_me');
	$('#save').removeClass('disabled');
	break;
```

Actions to be taken by the state transition is as:

 * *Initialization* - On initialization, clear the *idleTimer*, *clock*, wipe the editor's content, and kill all animations.
    * Remove the creeping border used by the *idleTimer* by removing the *danger* and *danger-short* class.  Also, remove the *blink_me* class associated with the effect of session completion.
    * Disable the save button, only allow saving when the session is completed. 
 * *Started* - Application is started, cache the starting time to *startTimer*, used to calculate session duration.  It also starts the clock tick, indicated by the *tickType* handler, which will be explained in the next section.
 * *Warning* - 3 seconds prior to the timeout, the warning period starts.  Adding the *danger* class to the body, readying the creeping border and mark set the *idleTimer* to the remaining time of 3 seconds.
    * When this *idleTimer* hits, call *StateUpdate* method to set the state to be *state.TimedOut*.  Unless otherwise reset by *inputType* event handler. 
 * *TimedOut* - When the application is timed out, just simply reset it back to the *Initialized* state.
 * *Completed* - Once the session is completed (identified by clock tick and input type handlers), clear only the *idleTimer* and let the user continue to use this application.  
    * If there is lingering animation, kill it.  
    * Blink the word/time limit, letting the user know the session is over.
    * Enable the save button to let the user export the completed content as a Microsoft Word document.

###### Clock Tick and Editor Input Handlers ######

Whenever the clock ticks and the editor has been updated, the *tickType* and *inputType* event handlers will be fired.  Instead of performing an if-condition check within the event handlers, it's more efficient to write both sets of event handlers and just refer to them, something JavaScript can do easily and cheaply.

As indicated by the **Editor Page**'s *Initialization*, if the session is set to be timed, then the *tickType* is set to *TickAndBail* (regular clock tick and bail if the limit is reached), and *inputType* is set to *Word* (regular word update); if the session is set to be words, then the *tickType* is set to *Tick* (regular clock tick) and *inputType* is set to *WordAndBail* (regular word update and bail if the limit is reached).

``` js
// On clock tick
function Tick() {
	// Display time difference
	var sec = (new Date() - startTime) / 1000;
	$('#sessionDuration').html(`${Math.floor(sec / 60)}:${$.LeadingZero('00', Math.floor(sec % 60))}`);
	return sec;
}
function TickAndBail() {
	// Display time difference
	var sec = Tick();

	// Check for end of timer
	if (sec >= (config.Session.Limit * 60)) {
		// Time up, add effect
		StateUpdate(state.Completed);
	}
}
```

The clock ticks every second, On the clock tick, finding out how many seconds had passed since the application started, as cached in *startTimer* variable.  A simple incrementing counter would do, but just in case there is an issue with delays.  Then, the number of seconds spent is shown to the footer (documented later) in a truncated form.

The only difference between two handlers clock tick handler is whether to bail or not, so, *TickAndBail* will just call *Tick* to display the overall clock, then if the number of seconds exceeds the session limit, call the *StateUpdate* method to update the application state to be *state.Completed*.

``` js
// Checking for word update
function Word(wordCount) {
	// Check for simple case
	if ((wordCount <= 0) || (cur == state.Completed))
		return;

	// If not yet started, start
	if (cur == state.Initialized)
		StateUpdate(state.Started);

	// Set idle timer on word update
	clearTimeout(idleTimer);
	$('body').removeClass('danger danger-short');
	if (config.IdleLimit >= 3)
		idleTimer = setTimeout(StateUpdate, (config.IdleLimit - 3) * 1000, state.Warning);
	else {
		$('body').addClass('danger-short');
		idleTimer = setTimeout(StateUpdate, config.IdleLimit * 1000, state.TimedOut);
	}
}
// Check to see if word limit hits
function WordAndBail(wordCount) {
	// Check for initializing
	Word(wordCount);

	// Check for end of timer
	if (wordCount >= config.Session.Limit) {
		// Word limit up, add effect
		StateUpdate(state.Completed);
	}
}
```

Whenever the [CKEditor 5](https://ckeditor.com/ckeditor-5/)'s Word Count plug-in receives an update, one of these two-word event handlers will be fired.  As the clock ticks, the primary feature of the word event is to start the application or reset the timer. 

If the application is in the *Initialized* state and a word has been entered, use the *StateUpdate* method to shift the application to the *Started* state.  Then, reset the *idleTimer* by clearing the current timer, remove the *danger* and *danger-short* class, then add the new timer for whatever it was set in the configuration.

Since the default timing out animation is set to be 3 seconds, so if the *config.IdleLimit* is greater than such, set the timer to be 3 seconds less than the limit, use *danger* class then get into the warning state when timer hits.  The warning state will then set the *idleTimer* to be 3 seconds, if ticks, transition to *TimedOut* state. If the timer is 3 seconds or less, use the *danger-short* class and hit the *TimedOut* state when ticks.

Just like the timer, the *WordAndBail* function uses the *Word* method to reset *idleTimer*, but check for *config.Session.Limit*.  If the limit is reached, transition to the *Completed* state.  Note that the *Word* method does not update the user interface to inform the user how many words had been entered, it is handled by the [CKEditor 5](https://ckeditor.com/ckeditor-5/)'s *Word Count* plug-in's *onUpdate* event handler.  It can exist in either location, this is just a design choice. 

The entire application relies on the clock tick and words update event handlers to transition this application between states, which uses the *StateUpdate* method to reinitialize certain components.

###### Editor Footer ######

The footer bar of the **Editor Page** shows the current status.  After much thoughts, a decision was made to simplify the UI to minimize distraction to the user.  So, only the current word count and session duration was listed in the form of:

``` html
<footer class="">
	Word Count: <span id="wordCount">[x]</span><span id="wordLimit"></span> | Session Duration: <span id="sessionDuration">0:00</span><span id="timeLimit"></span>
</footer>
```

As documented earlier, either the *wordLimit* or the *timeLimit* will be shown depending on the configuration.  The *wordCount* and *sessionDuration* will be shown regardless, updated every second and word update.  Other information can be displayed here, such as current application state, *idleTimer* vs *Idle Limit* counter, whether or not the content was saved...  But, for simplicity's sake, they are opted not to be implemented.

``` js
function Initialize() {
	...

	$('#timeLimit').html('');
	if (config.Session.Type == 'minutes') {
		tickType = TickAndBail;
		inputType = Word;
		$('#timeLimit').html(`/${config.Session.Limit}:00`);
	} else {
		tickType = Tick;
		inputType = WordAndBail;
		$('#wordLimit').html(`/${config.Session.Limit} Words`);
	}

	...
}
```

The *wordCount* is provided by [CKEditor 5](https://ckeditor.com/ckeditor-5/)'s *Word Count* *onUpdate* event handler.  The plug-in trims the format and special characters, only leave words separated by spaces.

``` js
function Initialize() {
	...

	// Initialize editor
	ClassicEditor
		.create(document.querySelector('#editor'), {
			...
			wordCount: {
				onUpdate: stats => {
					// Prints the current content statistics
					$('#wordCount').html(stats.words);
					$('#charCount').html(stats.characters);
					...
				}
			},
			...
		});

	...
}
```

The *sessionDuration* is updated by the clock tick timer, *Tick*.  It takes the difference between the current time and application start time (cached in *StateUpdate (state.Started)* handler).

``` js
// On clock tick
function Tick() {
	// Display time difference
	var sec = (new Date() - startTime) / 1000;
	$('#sessionDuration').html(`${Math.floor(sec / 60)}:${$.LeadingZero('00', Math.floor(sec % 60))}`);
	return sec;
}
```

### Contributing ###

Please consider attending [All Things Open](https://www.allthingsopen.org/) convention to learn about how to start your own open-sourced project or be a contributor.  Anyone is welcomed to pull and modify this application as they wish, just make sure you do not violate the terms of [MIT license](https://opensource.org/licenses/MIT) used by this application.


### License ###

This application uses tons of [MIT license](https://opensource.org/licenses/MIT) libraries, thus the huge installation package.  The original codes created by the author are free to distribute or reuse.