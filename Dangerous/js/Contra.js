﻿$(document).ready(function () {

   // Initialize Control code	
   var inputIndex = 0, contra = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65, 13];

   // Detect keystroke
   $(document).keydown(function (e) {
      if (e.keyCode == contra[inputIndex]) {
         if (++inputIndex >= contra.length) {
            // Match, trigger form creation
            inputIndex = 0;

            // If Contra function exists, call it
            if (typeof Contra == 'function')
               Contra();
            else
               console.log('Contra code accepted, nothing to call.');
         }
      }
      else {
         // Not correct, reset
         inputIndex = 0;
      }
   });
});