// Electron IPC call handler
const { ipcRenderer } = require('electron')

// Page Initialization Methods
{
	// Shared initialization method
	function SharedInitialization() {
		// Enable tooltip: https://atomiks.github.io/tippyjs/v6/constructor/
		tippy('[data-bs-toggle]', {
			content: (reference) => reference.getAttribute('data-title'),
			allowHTML: true,
		});

		// Register button event
		$('#light').bind('click', function () {
			// Toggle appearance
			config.LightButton.state = !config.LightButton.state;

			// Toggle light state
			if (config.LightButton.state) {
				// Light theme
				$('body, body div:first').removeClass(config.LightButton.darkTheme).addClass(config.LightButton.lightTheme);
				// Toggle button
				$(this).find('svg').attr('data-prefix', config.LightButton.onPrefixs);
			} else {
				// Dark theme
				$('body, body div:first').removeClass(config.LightButton.lightTheme).addClass(config.LightButton.darkTheme);
				// Toggle button
				$(this).find('svg').attr('data-prefix', config.LightButton.offPrefix);
			}
		});
		$('#fullscreen').bind('click', function () {
			// Toggle appearance
			config.FullButton.state = !config.FullButton.state;

			// Toggle full screen
			ipcRenderer.send('max-message', config.FullButton.state);
			$(this).find('svg').attr('data-icon', (config.FullButton.state ? config.FullButton.onIcon : config.FullButton.offIcon));

			/*
			if (config.FullButton.state) {
				// Full screen
				$('body').fullscreen();
				$(this).find('svg').attr('data-icon', config.FullButton.onIcon);
			} else {
				// Collapse
				$.fullscreen.exit();
				$(this).find('svg').attr('data-icon', config.FullButton.offIcon);
			}
			*/
		});
		$('#blur').bind('click', function () {
			// Toggle editor display text style
			config.BlurButton.state = !config.BlurButton.state;

			var blurClass = config.BlurButton.strength == 1 ? 'blurry-text' :
				config.BlurButton.strength == 2 ? 'blurrier-text' : 'blurriest-text';
			if (config.BlurButton.state) {
				$('form').removeClass('text-dark').addClass(blurClass);
				$(this).find('svg').attr('data-icon', config.BlurButton.onIcon);
			} else {
				$('form').removeClass('blurry-text blurrier-text blurriest-text').addClass('text-dark');
				$(this).find('svg').attr('data-icon', config.BlurButton.offIcon);
			}
		});
		$('#blurText').bind('change', function () {
			// On value change, enable the blur
			$('#blurOption').prop('checked', true);
		});
		$('#spellCheck').bind('click', function () {
			// Toggle editor display text style
			config.SpellCheck.state = !config.SpellCheck.state;
			$('form').attr('spellcheck', config.SpellCheck.state ? 'true' : 'false');
			$(this).find('svg').attr('data-icon', config.SpellCheck.state ? config.SpellCheck.onIcon : config.SpellCheck.offIcon);
		});
		$('#save').bind('click', function () {
			// Map to Electron ^o^
			SaveToWord();
		});
		$('#reset').bind('click', function () {
			// Call to reset content
			config.HasPopup = true;
			bootbox.confirm({
				message: 'Are you sure you want reset this editor?',
				closeButton: false,
				centerVertical: true,
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger'
					}
				},
				callback: function (result) {
					config.HasPopup = false;
					if (result)
						StateUpdate(state.Initialized);
				}
			});
		});
		$('#config').bind('click', function () {
			// Check for easy case
			if (config.HasPopup) return;

			// Go back home
			config.HasPopup = true;
			bootbox.confirm({
				message: 'Are you sure you want to bail out of Editor Mode?',
				closeButton: false,
				centerVertical: true,
				buttons: {
					confirm: {
						label: 'Yes',
						className: 'btn-success'
					},
					cancel: {
						label: 'No',
						className: 'btn-danger'
					}
				},
				callback: function (result) {
					config.HasPopup = false;
					if (result)
						Proceed(config.ConfigURI);
				}
			});
		});
		$('#editor').bind('click', function () {
			// Go back home
			if (confirm('Are you sure you want to switch to Editor Mode without using current configuration?'))
				Proceed(config.EditorURI);
		});
		$('#devTool').bind('click', function () {
			// Open the dev tool
			ipcRenderer.send('open-dev-tool-message', null);
		})

		// Load configuration
		var cache = GetEntry(config.ConfigName());
		if (cache != null) {
			if (cache.LightTheme)
				$('#light').trigger('click');
			config.BlurButton.strength = cache.BlurStrength;
			if (cache.BlurButton)
				$('#blur').trigger('click');
			$('#blurOption').prop('checked', cache.BlurButton);
			$('#blurText').val(config.BlurButton.strength);
			if (config.FullButton.state != cache.FullScreen)
				$('#fullscreen').trigger('click');
			else
				$('#fullscreen').find('svg').attr('data-icon', ((config.FullButton.state = cache.FullScreen) ? config.FullButton.onIcon : config.FullButton.offIcon));
			$('#spellcheckOn').prop('checked', cache.SpellCheck);
			if (cache.SpellCheck)
				$('#spellCheck').trigger('click');

			// Set config value
			config.IdleLimit = cache.IdleLimit;
			$('#idle').val(config.IdleLimit);
			config.Session.Type = cache.SessionType;
			$('input[type="radio"][value="' + config.Session.Type + '"]').trigger('click');
			config.Session.Limit = cache.SessionLimit;
			$('#' + config.Session.Type).val(config.Session.Limit);
		}

		// Detect keydown
		$(document).on('keydown', (data) => {
			// Detect keydown
			// console.log(data.keyCode);
			switch (data.keyCode) {
				case 68:
				case 82:
					// Ctrl + D/R, trigger state reset
					if (data.ctrlKey) {
						$('#reset').trigger('click');
						data.preventDefault();
						data.stopPropagation();
					}
					return;
				case 83:
					// Ctrl + S, trigger save
					if (data.ctrlKey) {
						$('#save').trigger('click');
						data.preventDefault();
						data.stopPropagation();
					}
					return;
				case 49:
					// Ctrl + 1, trigger spell check
					if (data.ctrlKey) {
						$('#spellCheck').trigger('click');
						data.preventDefault();
						data.stopPropagation();
					}
					return;
				case 50:
					// Ctrl + 2, trigger blur
					if (data.ctrlKey) {
						$('#blur').trigger('click');
						data.preventDefault();
						data.stopPropagation();
					}
					return;
				case 51:
					// Ctrl + 3, trigger light button
					if (data.ctrlKey) {
						$('#light').trigger('click');
						data.preventDefault();
						data.stopPropagation();
					}
					return;
				case 52:
				case 53:
				case 54:
				case 55:
				case 56:
				case 57:
				case 48:
					// Ctrl + 5 ~ 0, nada
					if (data.ctrlKey) {
						data.preventDefault();
						data.stopPropagation();
					}
					break;
				case 112:
					// F1, info button
					// this.onF1();
					$('#config').trigger('click');
					data.preventDefault();
					data.stopPropagation();
					break;
				case 122:
					// F11, full screen
					$('#fullscreen').trigger('click');
					data.preventDefault();
					data.stopPropagation();
					return;
				case 123:
					// F12, debug console
					$('#devTool').trigger('click');
					data.preventDefault();
					data.stopPropagation();
					return;
			}
		});
	}
}

// Helper Method
{
	// Proceed to another page
	function Proceed(uri) {
		window.location.assign(uri);
	}

	// IPC Callback ^o^
	ipcRenderer.on('word-export-reply', (event, arg) => {
		console.log(arg) // prints "done"
		// Resume clock 
	})
}

// Local Storage Functionalities
{
	// Get all cached entries
	function GetAllEntries() {
		return GetAllKeys().map(function (key) { return { Key: key, Value: GetEntry(key) }; });
	}

	// Get all cached keys
	function GetAllKeys() {
		var entries = [];
		for (var key in localStorage) {
			if (key.startsWith(config.ProjID))
				entries.push(key);
		}
		return entries;
	}

	// Clear a cached entry if Id is provided, clear all otherwise
	function ClearCache(key) {
		if (!$.isNullOrEmpty(key))
			localStorage.removeItem(key);
		else {
			for (var key in localStorage)
				localStorage.removeItem(key);
		}
	}

	// Add an entry to cache, option to overwrite key
	function Push(entry, key = null) {
		var key = $.isNullOrEmpty(key) ? config.NewPageID() : key;
		localStorage.setItem(key, JSON.stringify(entry));
		return GetEntry(key);
	}

	// Get an entry from the cache
	function GetEntry(key) {
		// Simple verification
		if (localStorage[key] === undefined)
			return null;
		return JSON.parse(localStorage[key]);
	}
}